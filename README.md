# Oracle Table Synchronizer

As of October 2019, we are trying to star managing dynamically a set of DNS zones with the project DNS manager. One of the first steps of transition from the old way we managed DNS names the new one is comprised of retiring an Oracle database that holds a view with all the DNS entries.

For this, we need to first replicate this information into Openshift that is where this project comes into action. This project is responsible for building a Docker image that contains a program that will create a set of CRs in Openshift according to the entries of the view ```ZONE_WEB``` in the Oracle database ```cerndb1```. These CRs will then be used to populate DNS zones.

## Parametrized values
In order to interact with this parametrized values, the only requirement is to add the pertinent flag during the execution (e.g. ```-number-of-crs 5```)

**Mandatory values**:
- **oracle-connection-string**: (e.g ```bob/password@pdb-s.cern.ch:10121/PDB_CERNDB1.cern.ch```). This values is the oracle connection string that will allow the program to connect himself to the Oracle database;
- **domain**: (e.g ```docs.cern.ch=internal.docs.dns-manager.cern.ch```). This value represents the domain used to build the CNAME entreis and the API group of the CRs that are going to be created, this value can be inputed multiple times.
For instance if you specify `--domain docs.cern.ch=internal.docs.dns-manager.cern.ch --domain docs.cern.ch=external.docs.dns-manager.cern.ch --domain webtest.cern.ch=internal.webtest.dns-manager.cern.ch` this program will generate 10 DNSEndpoint CRs of groups `internal.docs.dns-manager.cern.ch`, `external.docs.dns-manager.cern.ch` and `internal.webtest.dns-manager.cern.ch` splited among the 10 CRs will be all the records in the view `ZONE_WEB` pointing to CNAMES that end with the respective domains, in this case `docs.cern.ch`, `docs.cern.ch` and `webtest.cern.ch`.

Optional values:

- **number-of-crs**: (e.g ```5```) Default value is ```10```. This value is total number of CRs that will be created to hold the mutiple DNS entries among them. Entries are assigned to a a CR using the ```MD5Hash(alias) % number-of-crs```;
- **namespace**: (e.g ```myproject``` ) Default value is ```paas-infra-dnsmanager```;
- **kind**: (e.g ```MySpecialCRD```) Default value is ```DNSEndpoint```. This value represents the ```kind``` of the CRs that are going to be created;
- **version**: (e.g ```v1alpha1```) Default value is ```v1alpha1```. This value is the ApiVersion of the CRs that are going to be created
- **name**: (e.g ```fancy-name```) Default value is ```record```. This will be the prefix of the name of each CR created concatenated with their number (e.g record-1, record-2...)

## Service accounts

The service account that runs this program will need to have create and patch the CRs that were inputed in the parameters. See example in [dns-manager](https://gitlab.cern.ch/paas-tools/infrastructure/dns-manager/blob/master/chart/charts/oracle-table-sync/templates/rbac.yaml) where this program is deployed.

## Deployment 
This project will be deployed by the helm chart named ```oracle-table-sync``` present in [this repo](https://gitlab.cern.ch/paas-tools/infrastructure/dns-manager). The namespace used to be deployed is by default ```paas-infra-dnsmanager```, in all the clusters.