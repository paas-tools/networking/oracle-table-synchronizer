package main

import (
	"crypto/md5"
	"encoding/binary"
	"encoding/json"
	"flag"
	"fmt"
	"strings"

	"database/sql"

	_ "gopkg.in/goracle.v2"

	log "github.com/sirupsen/logrus"

	endpt "github.com/kubernetes-incubator/external-dns/endpoint"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/runtime/serializer"
	types "k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
)

var (
	domainAPIGroup   arrayFlags
	config           *rest.Config
	connectionString = flag.String("oracle-connection-string", "default", "Connection string to oracle database")
	// When testing the performance of External-DNS we discovered that if we ask OpenShift to create a CR that holds 15000 endpoints
	// the request is considered too large. We picked 10 CRs to make sure there was enough space in each CR and that each one would be more or less 50% full.
	numberOfCrs    = flag.Int("number-of-crs", 10, "Number of CRs that are going to be created to host all the entries in the database")
	namespace      = flag.String("namespace", "paas-infra-dnsmanager", "Namespace where the CRs are going to be created")
	kind           = flag.String("kind", "DNSEndpoint", "Kind of the CRs that are going to be created")
	resourcePlural = strings.ToLower(*kind) + "s"
	version        = flag.String("version", "v1alpha1", "ApiVersion of the CRs that are going to be created")
	nameOfRecords  = flag.String("name", "record", "Name that the CRs will have that will be concatenated with their number e.g record-1, record-2...")
)

func main() {

	flag.Var(&domainAPIGroup, "domain", "String should follow the convention <domain>=<group> where <domain> is a normal dns domain (web.cern.ch, webtest.cern,ch) and <group> is the api group of the CRs that this program will create")

	// Parses the command line into the defined flags
	flag.Parse()

	domainGroupsMap := map[string][]string{}
	for i := 0; i < len(domainAPIGroup); i++ {
		domainGroup := strings.Split(domainAPIGroup[i], "=")
		if val, ok := domainGroupsMap[domainGroup[0]]; ok {
			// Append group to list of groups
			domainGroupsMap[domainGroup[0]] = append(val, domainGroup[1])
		} else {
			// ADD key domain -> group
			domainGroupsMap[domainGroup[0]] = []string{domainGroup[1]}
		}
	}

	if *connectionString == "default" {
		log.Fatal("A connectoin string was not provided, please provide one")
	}

	var err error
	// creates the in-cluster config
	config, err = rest.InClusterConfig()
	if err != nil {
		panic(err.Error())
	}
	config.APIPath = "/apis"
	config.NegotiatedSerializer = serializer.WithoutConversionCodecFactory{CodecFactory: scheme.Codecs}
	config.UserAgent = rest.DefaultKubernetesUserAgent()

	// Open connection to DB in connection string
	db, err := sql.Open("goracle", *connectionString)
	if err != nil {
		log.Fatal(err)
		return
	}
	defer db.Close()

	log.Info("Going to preform query")
	// Here I thought about using WHERE to filter the rows of TYPE "EOSCONTAINERS" but this removes
	// the rows that have NULL TYPE which we don't want to lose
	rows, err := db.Query("SELECT * FROM \"WEBREG\".\"ZONE_WEB_WITH_TYPE\"")
	if err != nil {
		log.Fatal("Error running query: %v", err)
		return
	}
	defer rows.Close()

	// Filling the zone structure
	// This map will contain as keys the alias that we want to create
	// (e.g. bob, alice, eve) and in the values it will be a list
	// of all targets that we want to point to (e.g. oonodelb.cern.ch).
	// ExternalDNS will then create a DNS CNAME entry for each target in the list of targets.
	zone := map[string][]string{}

	var alias, name, domain, siteType string
	for rows.Next() {
		if err := rows.Scan(&alias, &name, &domain, &siteType); err != nil {
			log.Fatal(err)
		}
		alias = strings.ToLower(alias)
		name = strings.ToLower(name)
		domain = strings.ToLower(domain)
		siteType = strings .ToLower(siteType)
		// Filter out rows:
		// - Of sitetype eoscontainer as those will Webeos projects that were moved to Webeos OKD4 infra
		// - Of target paas-apps-shard-1 as those will PaaS projects that were moved to PaaS OKD4 infra
		// - Of target drupal-apps-shard-1 as those will Drupal projects that were moved to Drupal OKD4 infra
		// - Of target app-catalogue-apps-shard-1 as those will App Cat projects that were moved to App Catalogue OKD4 infra
		if siteType == "eoscontainer" || name == "paas-apps-shard-1" || name == "drupal-apps-shard-1" || name == "app-catalogue-apps-shard-1" {
			continue
		}
		if val, ok := zone[alias]; ok {
			zone[alias] = append(val, name+domain)
		} else {
			zone[alias] = []string{name + domain}
		}
	}

	log.Info("Zone struture built and populated")

	// Processing the zones
	generateCRs(domainGroupsMap, zone)

}

func generateCRs(domainGroupsMap map[string][]string, zone map[string][]string) {
	for domain, crdGroups := range domainGroupsMap {
		for _, group := range crdGroups {
			log.Info("Going to process ", group, " with ", len(zone), " aliases")

			var endpoints = make([][]*endpt.Endpoint, *numberOfCrs)

			for alias, targets := range zone {

				// We will use the result of MD5Hash(alias) % numberOfCrs to determine in which CR we are going to store this record.
				// We do this to first evenly distribute the different DNS records among the different CRs and to also avoid the problem
				// that if a record is removed we will for sure remove it from the CRs the next time this program runs
				hash := md5.Sum([]byte(alias))
				key := binary.BigEndian.Uint16(hash[:]) % uint16(*numberOfCrs)

				endpoints[key] = append(endpoints[key], createEndpoint(alias+"."+domain, 60, targets))
			}
			for i := 0; i < *numberOfCrs; i++ {
				name := fmt.Sprintf("%s-%d", *nameOfRecords, i+1)
				dnsEndpoint := createDNSEndpoint(name, endpoints[i])
				dnsEndpoint.APIVersion = group + "/" + *version
				dnsEndpoint.Kind = *kind

				createResource(group, name, dnsEndpoint)
			}
		}
	}
}

// Function that will try to create or patch the CRs in openshift using the REST API
func createResource(group string, name string, dnsEndpoint *endpt.DNSEndpoint) {
	// Writing resource to JSON
	data, err := json.MarshalIndent(dnsEndpoint, "", " ")
	if err != nil {
		panic(err)
	}

	// Setting up the group and version for client config
	config.GroupVersion = &schema.GroupVersion{Group: group, Version: *version}

	// Creates the clientset
	clientset, err := rest.RESTClientFor(config)
	if err != nil {
		panic(err.Error())
	}

	// Attempts to create the resource
	result := clientset.Post().
		Namespace(*namespace).
		Resource(resourcePlural).
		Body(data).
		Do()

	var status int
	result.StatusCode(&status)

	// In case the object already exists lets just patch the Endpoints of the resource
	if status == 409 {

		payload := []patchStringValue{{
			Op:    "replace",
			Path:  "/spec/endpoints",
			Value: dnsEndpoint.Spec.Endpoints,
		}}

		toSend, erro := json.Marshal(payload)
		if erro != nil {
			panic(err)
		}

		result = clientset.Patch(types.JSONPatchType).
			Namespace(*namespace).
			Resource(resourcePlural).
			Name(name).
			Body(toSend).
			Do()
		result.StatusCode(&status)
	}
	// Final result of the operation
	if status != 201 && status != 200 {
		log.Fatal(result.Error().Error())
	}
	log.Info("Created/Updated CR of group ", group, " named ", name, " with ", len(dnsEndpoint.Spec.Endpoints), " endpoints")
}