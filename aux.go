package main

import (
	endpt "github.com/kubernetes-incubator/external-dns/endpoint"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)


// Type necessary to hold all the group flags that might be in the parameters
type arrayFlags []string

// Necessary function to be implemented
func (i *arrayFlags) String() string {
    return "my string representation"
}

func (i *arrayFlags) Set(value string) error {
    *i = append(*i, value)
    return nil
}


//  patchStringValue specifies a patch operation for a string.
type patchStringValue struct {
	Op    string `json:"op"`
	Path  string `json:"path"`
	Value []*endpt.Endpoint `json:"value"`
}

// Function to create/initialize the DNSEndpoint structure
func createDNSEndpoint(name string, endpoints []*endpt.Endpoint) *endpt.DNSEndpoint {
	return &endpt.DNSEndpoint{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: *namespace,
		},
		Spec: endpt.DNSEndpointSpec{
			Endpoints: endpoints,
		},
	}
}

// Function to create/initialize the DNSEndpoint structure
func createEndpoint(hostname string, timeToLive int64, target []string) (endpoint *endpt.Endpoint) {
	endpoint = new(endpt.Endpoint)
	endpoint.DNSName = hostname
	endpoint.Targets = target
	endpoint.RecordTTL = endpt.TTL(timeToLive)
	endpoint.RecordType = "CNAME"
	return endpoint
}